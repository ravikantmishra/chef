<?php

include_once "../class/common.php";
if (isset($_GET['action']) && $_GET['action'] == 'getinfo') {
    $data = getinfo($_POST);
    echo $data;
} else if (isset($_GET['action']) && $_GET['action'] == 'addinfo') {
    $data = addinfo($_POST, $_GET['input_board']);
    $data = json_decode($data);
    if ($data->status == 1) {
        echo json_encode(array('status' => 1, 'msg' => 'Saved successfully.'));
    } else {
        echo json_encode(array('status' => 0, 'msg' => 'Some error has occured.', 'error' => $data->message));
    }
} else {
    
}

function addinfo($val, $inputBoard)
{
    try {
        $obj = new common();
        if ($inputBoard == 4) { //Soupe Special
            $loop = 1;
        }

        if ($inputBoard == 2) { //Members only
            $loop = 3;
        }

        if ($inputBoard == 1) {//Daily special
            $loop = 7;
        }
        
        if ($inputBoard == 3) {//Asian food special
            $loop = 5;
        }

        //delete previous records
        $obj->deleteRecordes($inputBoard, date('Y-m-d'));

        for ($i = 1; $i <= $loop; $i++) {
            $arrData = array(
                'dish_name' => $val['dish_' . $i],
                'input_board' => empty($inputBoard) ? 0 : $inputBoard,
                'member_price' => isset($val['member_' . $i]) ? $val['member_' . $i] : 0,
                'guest_price' => isset($val['guest_' . $i]) ? $val['guest_' . $i] : 0,
                'created_on' => date('Y-m-d')
            );
            $id = $obj->createInsert('menu_items', $arrData);
        }
        return json_encode(array('status' => 1, 'message' => $id));
    } catch (Exception $e) {
        return json_encode(array('status' => 0, 'message' => $e->getMessage()));
    }
}

function getinfo($v)
{
    $obj = new common();
    $data = $obj->getMenuItems($v['input_board'], date('Y-m-d'));

    if ($v['input_board'] == 1) {
        return board_one($data);
    } elseif ($v['input_board'] == 2) {
        return board_two($data);
    } elseif ($v['input_board'] == 3) {
        return board_three($data);
    } elseif ($v['input_board'] == 4) {
        return board_four($data);
    }
}

function board_one($data)
{
    $var = "
    <thead>
        <tr>
            <th width='5%'></th>
            <th width='40%'>Enter Specials Input Board</th>
            <th width='5%'></th>
            <th width='20%'>Members $</th>    
            <th width='5%'></th>
            <th width='20%'>Guets $</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td align='right'>1</td>
            <td><input type='text' class='input_first' value='" . (isset($data[0]['dish_name']) ? $data[0]['dish_name'] : '') . "' name='dish_1' id='dish_1' /></td>
            <td></td>
            <td><input type='text' class='input_two' value='" . (isset($data[0]['member_price']) ? $data[0]['member_price'] : '') . "' name='member_1' id='member_1' /></td>
            <td></td>
            <td><input type='text' class='input_two' value='" . (isset($data[0]['guest_price']) ? $data[0]['guest_price'] : '') . "' name='guest_1' id='guest_1' /></td>
        </tr>
        <tr><td colspan='6'></td></tr>
        <tr class='non_special'>
            <td align='right'>2</td>
            <td><input type='text' class='input_first' value='" . (isset($data[1]['dish_name']) ? $data[1]['dish_name'] : '') . "' name='dish_2' id='dish_2' /></td>
            <td></td>
            <td><input type='text' class='input_two' value='" . (isset($data[1]['member_price']) ? $data[1]['member_price'] : '') . "' name='member_2' id='member_2' /></td>
            <td></td>
            <td><input type='text' class='input_two' value='" . (isset($data[1]['guest_price']) ? $data[1]['guest_price'] : '') . "' name='guest_2' id='guest_2' /></td>
        </tr>
        <tr class='non_special'><td colspan='6'></td></tr>
        <tr class='non_special'>
            <td align='right'>3</td>
            <td><input type='text' class='input_first' value='" . (isset($data[2]['dish_name']) ? $data[2]['dish_name'] : '') . "' name='dish_3' id='dish_3' /></td>
            <td></td>
            <td><input type='text' class='input_two' value='" . (isset($data[2]['member_price']) ? $data[2]['member_price'] : '') . "' name='member_3' id='member_3' /></td>
            <td></td>
            <td><input type='text' class='input_two' value='" . (isset($data[2]['guest_price']) ? $data[2]['guest_price'] : '') . "' name='guest_3' id='guest_3' /></td>
        </tr>
        <tr class='non_special'><td colspan='6'></td></tr>
        <tr class='non_special'>
            <td align='right'>4</td>
            <td><input type='text' class='input_first' value='" . (isset($data[3]['dish_name']) ? $data[3]['dish_name'] : '') . "' name='dish_4' id='dish_4' /></td>
            <td></td>
            <td><input type='text' class='input_two' value='" . (isset($data[3]['member_price']) ? $data[3]['member_price'] : '') . "' name='member_4' id='member_4' /></td>
            <td></td>
            <td><input type='text' class='input_two' value='" . (isset($data[3]['guest_price']) ? $data[3]['guest_price'] : '') . "' name='guest_4' id='guest_4' /></td>
        </tr>
        <tr class='non_special'><td colspan='6'></td></tr>
        <tr class='non_special'>
            <td align='right'>5</td>
            <td><input type='text' class='input_first' value='" . (isset($data[4]['dish_name']) ? $data[4]['dish_name'] : '') . "' name='dish_5' id='dish_5' /></td>
            <td></td>
            <td><input type='text' class='input_two' value='" . (isset($data[4]['member_price']) ? $data[4]['member_price'] : '') . "' name='member_5' id='member_5' /></td>
            <td></td>
            <td><input type='text' class='input_two' value='" . (isset($data[4]['guest_price']) ? $data[4]['guest_price'] : '') . "' name='guest_5' id='guest_5' /></td>
        </tr>
        <tr class='non_special'><td colspan='6'></td></tr>
        <tr class='non_special'>
            <td align='right'>6</td>
            <td><input type='text' class='input_first' value='" . (isset($data[5]['dish_name']) ? $data[5]['dish_name'] : '') . "' name='dish_6' id='dish_6' /></td>
            <td></td>
            <td><input type='text' class='input_two' value='" . (isset($data[5]['member_price']) ? $data[5]['member_price'] : '') . "' name='member_6' id='member_6' /></td>
            <td></td>
            <td><input type='text' class='input_two' value='" . (isset($data[5]['guest_price']) ? $data[5]['guest_price'] : '') . "' name='guest_6' id='guest_6' /></td>
        </tr>
        <tr class='non_special'><td colspan='6'></td></tr>
        <tr class='non_special'>
            <td align='right'>7</td>
            <td><input type='text' class='input_first' value='" . (isset($data[6]['dish_name']) ? $data[6]['dish_name'] : '') . "' name='dish_7' id='dish_7' /></td>
            <td></td>
            <td><input type='text' class='input_two' value='" . (isset($data[6]['member_price']) ? $data[6]['member_price'] : '') . "' name='member_7' id='member_7' /></td>
            <td></td>
            <td><input type='text' class='input_two' value='" . (isset($data[6]['guest_price']) ? $data[6]['guest_price'] : '') . "' name='guest_7' id='guest_7' /></td>
        </tr>
        <tr class='non_special'><td colspan='6'></td></tr>
        <tr>
            <td align='right'></td>
            <td></td>
            <td></td>
            <td align='right'><input type='reset' name='Cancel' value='Cancel' class='button' /></td>
            <td></td>
            <td><input type='submit' name='save' value='Submit' class='button' id='save_data' /></td>
        </tr>
    </tbody>
    ";

    return $var;
}

function board_two($data)
{
    $var = "
    <thead>
        <tr>
            <th width='5%'></th>
            <th width='40%'>Enter Specials Input Board</th>
            <th width='5%'></th>
            <th width='20%'></th>    
            <th width='5%'></th>
            <th width='20%'>Members $</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td align='right'>1</td>
            <td><input type='text' class='input_first' value='" . (isset($data[0]['dish_name']) ? $data[0]['dish_name'] : '') . "' name='dish_1' id='dish_1' /></td>
            <td></td>
            <td></td>
            <td></td>
            <td><input type='text' class='input_two' value='" . (isset($data[0]['member_price']) ? $data[0]['member_price'] : '') . "' name='member_1' id='member_1' /></td>
        </tr>
        <tr><td colspan='6'></td></tr>
        <tr class='non_special'>
            <td align='right'>2</td>
            <td><input type='text' class='input_first' value='" . (isset($data[1]['dish_name']) ? $data[1]['dish_name'] : '') . "' name='dish_2' id='dish_2' /></td>
            <td></td>
            <td></td>
            <td></td>
            <td><input type='text' class='input_two' value='" . (isset($data[1]['member_price']) ? $data[1]['member_price'] : '') . "' name='member_2' id='member_2' /></td>
        </tr>
        <tr class='non_special'><td colspan='6'></td></tr>
        <tr class='non_special'>
            <td align='right'>3</td>
            <td><input type='text' class='input_first' value='" . (isset($data[2]['dish_name']) ? $data[2]['dish_name'] : '') . "' name='dish_3' id='dish_3' /></td>
            <td></td>
            <td></td>
            <td></td>
            <td><input type='text' class='input_two' value='" . (isset($data[2]['member_price']) ? $data[2]['member_price'] : '') . "' name='member_3' id='member_3' /></td>
        </tr>
        <tr class='non_special'><td colspan='6'></td></tr>
        <tr>
            <td align='right'></td>
            <td></td>
            <td></td>
            <td align='right'><input type='reset' name='Cancel' value='Cancel' class='button' /></td>
            <td></td>
            <td><input type='submit' name='save' value='Submit' class='button' id='save_data' /></td>
        </tr>
    </tbody>
    ";

    return $var;
}

function board_three($data)
{
    $var = "
    <thead>
        <tr>
            <th width='5%'></th>
            <th width='40%'>Enter Specials Input Board</th>
            <th width='5%'></th>
            <th width='20%'>Members $</th>    
            <th width='5%'></th>
            <th width='20%'>Guets $</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td align='right'>1</td>
            <td><input type='text' class='input_first' value='" . (isset($data[0]['dish_name']) ? $data[0]['dish_name'] : '') . "' name='dish_1' id='dish_1' /></td>
            <td></td>
            <td><input type='text' class='input_two' value='" . (isset($data[0]['member_price']) ? $data[0]['member_price'] : '') . "' name='member_1' id='member_1' /></td>
            <td></td>
            <td><input type='text' class='input_two' value='" . (isset($data[0]['guest_price']) ? $data[0]['guest_price'] : '') . "' name='guest_1' id='guest_1' /></td>
        </tr>
        <tr><td colspan='6'></td></tr>
        <tr class='non_special'>
            <td align='right'>2</td>
            <td><input type='text' class='input_first' value='" . (isset($data[1]['dish_name']) ? $data[1]['dish_name'] : '') . "' name='dish_2' id='dish_2' /></td>
            <td></td>
            <td><input type='text' class='input_two' value='" . (isset($data[1]['member_price']) ? $data[1]['member_price'] : '') . "' name='member_2' id='member_2' /></td>
            <td></td>
            <td><input type='text' class='input_two' value='" . (isset($data[1]['guest_price']) ? $data[1]['guest_price'] : '') . "' name='guest_2' id='guest_2' /></td>
        </tr>
        <tr class='non_special'><td colspan='6'></td></tr>
        <tr class='non_special'>
            <td align='right'>3</td>
            <td><input type='text' class='input_first' value='" . (isset($data[2]['dish_name']) ? $data[2]['dish_name'] : '') . "' name='dish_3' id='dish_3' /></td>
            <td></td>
            <td><input type='text' class='input_two' value='" . (isset($data[2]['member_price']) ? $data[2]['member_price'] : '') . "' name='member_3' id='member_3' /></td>
            <td></td>
            <td><input type='text' class='input_two' value='" . (isset($data[2]['guest_price']) ? $data[2]['guest_price'] : '') . "' name='guest_3' id='guest_3' /></td>
        </tr>
        <tr class='non_special'><td colspan='6'></td></tr>
        <tr class='non_special'>
            <td align='right'>4</td>
            <td><input type='text' class='input_first' value='" . (isset($data[3]['dish_name']) ? $data[3]['dish_name'] : '') . "' name='dish_4' id='dish_4' /></td>
            <td></td>
            <td><input type='text' class='input_two' value='" . (isset($data[3]['member_price']) ? $data[3]['member_price'] : '') . "' name='member_4' id='member_4' /></td>
            <td></td>
            <td><input type='text' class='input_two' value='" . (isset($data[3]['guest_price']) ? $data[3]['guest_price'] : '') . "' name='guest_4' id='guest_4' /></td>
        </tr>
        <tr class='non_special'><td colspan='6'></td></tr>
        <tr class='non_special'>
            <td align='right'>5</td>
            <td><input type='text' class='input_first' value='" . (isset($data[4]['dish_name']) ? $data[4]['dish_name'] : '') . "' name='dish_5' id='dish_5' /></td>
            <td></td>
            <td><input type='text' class='input_two' value='" . (isset($data[4]['member_price']) ? $data[4]['member_price'] : '') . "' name='member_5' id='member_5' /></td>
            <td></td>
            <td><input type='text' class='input_two' value='" . (isset($data[4]['guest_price']) ? $data[4]['guest_price'] : '') . "' name='guest_5' id='guest_5' /></td>
        </tr>
        <tr class='non_special'><td colspan='6'></td></tr>
        <tr>
            <td align='right'></td>
            <td></td>
            <td></td>
            <td align='right'><input type='reset' name='Cancel' value='Cancel' class='button' /></td>
            <td></td>
            <td><input type='submit' name='save' value='Submit' class='button' id='save_data' /></td>
        </tr>
    </tbody>
    ";

    return $var;
}

function board_four($data)
{
    $var = "
    <thead>
        <tr>
            <th width='5%'></th>
            <th width='40%'>Enter Specials Input Board</th>
            <th width='5%'></th>
            <th width='20%'>Members $</th>    
            <th width='5%'></th>
            <th width='20%'>Guets $</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td align='right'>1</td>
            <td><input type='text' class='input_first' value='" . (isset($data[0]['dish_name']) ? $data[0]['dish_name'] : '') . "' name='dish_1' id='dish_1' /></td>
            <td></td>
            <td><input type='text' class='input_two' value='" . (isset($data[0]['member_price']) ? $data[0]['member_price'] : '') . "' name='member_1' id='member_1' /></td>
            <td></td>
            <td><input type='text' class='input_two' value='" . (isset($data[0]['guest_price']) ? $data[0]['guest_price'] : '') . "' name='guest_1' id='guest_1' /></td>
        </tr>
        <tr class='non_special'><td colspan='6'></td></tr>
        <tr>
            <td align='right'></td>
            <td></td>
            <td></td>
            <td align='right'><input type='reset' name='Cancel' value='Cancel' class='button' /></td>
            <td></td>
            <td><input type='submit' name='save' value='Submit' class='button' id='save_data' /></td>
        </tr>
    </tbody>
    ";

    return $var;
}

