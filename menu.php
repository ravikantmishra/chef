<?php
include_once "class/common.php";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en"> 
    <head> 
        <title></title>
        <link href="css/style.css" type="" rel="stylesheet" /> 
        <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" /> 
    </head> 
    <body> 
        <div id="wrapper" class="dailyspecial">
            <h2 class="members_div double">Daily <span class="title_div">Specials</span></h2>
            <div class="table_content center">
                <table border="1" cellpadding="0" cellspacing="0" width="100%">
                    <thead>
                        <th class="menu_text">&nbsp;</th>
                        <th class="menu_box_space">&nbsp;</th>
                        <th class="member_price">Members</th>
                        <th class="menu_box_space">&nbsp;</th>
                        <th class="guest_price">Guests</th>
                    </thead>
                    <?php
                    $obj = new common();
                    $data = $obj->getMenuItems('1', date('Y-m-d'));
                    foreach($data as $val){
                        if(empty($val['dish_name'])):
                            continue;
                        endif;
                    ?>
                    <tbody>
                        <tr>
                            <td class="menu_text"><?php echo isset($val['dish_name']) ? $val['dish_name'] : 'N/A'; ?></td>
                            <td class="menu_box_space"></td>
                            <td class="member_price"><?php echo isset($val['member_price']) ? '$'.$val['member_price'] : 'N/A'; ?></td>
                            <td class="menu_box_space"></td>
                            <td class="guest_price"><?php echo isset($val['guest_price']) ? '$'.$val['guest_price'] : 'N/A'; ?></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>  
        </div> 
    </body> 
</html>